![node](https://img.shields.io/badge/nodejs-v12.14.1-83CD29.svg?style=flat-square)
![yarn](https://img.shields.io/badge/yarn-1.21.1-blue.svg?style=flat-square)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Messenger API

This project is build with React for front-side & Firebase as back-end api.
User should have to subscribe or login with a single email to have access to all messages posted and to send new message.
Messages are send and fetch in real-time.

## Project installation

#### 1. Config
To connect to Firebase, copy .env in root folder with credential
/.env

#### 1. Install
Use <strong>Node version 12.14.1</strong><br>
Proceed with the command <strong>"yarn install"</strong> in the root folder<br/>
Finally <strong>"yarn install"</strong> to run app in development <br>

App will run on port 3000: http://localhost:3000/

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
