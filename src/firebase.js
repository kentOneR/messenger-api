import firebase from 'firebase';

const config = {
    apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
    authDomain: "messenger-api-57366.firebaseapp.com",
    databaseURL: "https://messenger-api-57366.firebaseio.com",
    projectId: "messenger-api-57366",
    storageBucket: "messenger-api-57366.appspot.com",
    messagingSenderId: process.env.REACT_APP_FIREBASE_API_SENDER,
    appId: process.env.REACT_APP_FIREBASE_APP_ID,
    measurementId: process.env.REACT_APP_FIREBASE_MEASURE
};

firebase.initializeApp(config);

export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export const database = firebase.database();
export default firebase;
