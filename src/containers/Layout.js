import React, {useEffect, useState}	from 'react';
import PropTypes					from 'prop-types';

import Header 						from '../components/Header';
import style 						from '../scss/containers/layout.module.scss';

const Layout = (props) => {

	const [isMount, setIsMount] = useState(false);

	/**
	 * Hook: componentDidMount
	 */
	useEffect(() => {
		setIsMount(true);
	}, []);

	return (
		<>
			<Header isAuth={props.isAuth} isMount={isMount}/>
			<main className={style.layout}>
				{props.children}
			</main>
		</>
	)
};

Layout.propTypes = {
	children: PropTypes.element
};

export default Layout;
