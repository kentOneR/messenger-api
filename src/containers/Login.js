import React, {useState, useEffect} from 'react';
import firebase 					from '../firebase';

import {useStore} 					from '../hooks/store';
import {SIGN}						from '../hooks/actionTypes';

import Button 						from '../components/ui/Button';
import Input 						from '../components/ui/Input';
import Title 						from '../components/ui/Title';

import styles 						from '../scss/containers/login.module.scss';

import {checkValidity} 				from '../utils';

const Login = (props) => {

	const [state, setState] = useState({
		controls: {
			email: {
				elType: 'input',
				elConfig: {
					type: 'email',
					placeholder: 'Your email',
					autoComplete: 'email'
				},
				value: '',
				validation: {
					required: true,
					isEmail: true
				},
				valid: false,
				touched: false
			},
			username: {
				elType: 'input',
				elConfig: {
					type: 'text',
					placeholder: 'Your username',
					autoComplete: 'name'
				},
				value: '',
				validation: {
				  required: true
				},
				valid: false,
				touched: false
			  },
			password: {
				elType: 'input',
				elConfig: {
					type: 'password',
					placeholder: 'Your password',
					autoComplete: 'current-password'
				},
				value: '',
				validation: {
					required: true,
					minLength: 6
				},
				errorMsg: 'Must be at least 6 characters',
				valid: false,
				touched: false
			}
		},
		isSignup: true,
		error: ''
	});

	const [globalState, dispatch] = useStore(true),
		  {user}				  = globalState;


	/*
	 * Hook: trigger on user global state changed
	 * If user log: redirect to '/messages'
	*/
	useEffect(() => {
		if ('username' in user) {
			props.history.push('/messages');
		}
	}, [user, props]);

	const inputChangedHandler = (e, id) => {
		const updateState = {
			...state,
			controls: {
				...state.controls,
				[id]: {
				  ...state.controls[id],
				  value: e.target.value,
				  valid: checkValidity(e.target.value, state.controls[id].validation),
				  touched: true
				}
			}
		};

		setState({...updateState});
	};

	const onSubmitHandler = (e) => {
		e.preventDefault();

		const email 	= state.controls.email,
			  username	= state.controls.username,
			  password  = state.controls.password;


		if (!email.valid || !password.valid) return false;

		if (!state.isSignup) {
			firebase
				.auth()
				.signInWithEmailAndPassword(email.value, password.value)
				.then((res) => {
					res.user.getIdToken(/* forceRefresh */ true)
						.then((idToken) => {
						// Send token to your backend via HTTPS
						const data = res.user;
						data.token = idToken;

						dispatch(SIGN, res.user);
					  }).catch((error) => {
						console.warn(error);
						setState({
							...state,
							error: error.message
						});
					  });
				})
				.catch(error => {
					console.warn(error);
					setState({
						...state,
						error: error.message
					});
				});
		} else if (username.valid) {
			firebase
			.auth()
			.createUserWithEmailAndPassword(email.value, password.value)
			.then(() => {
				const user = firebase.auth().currentUser;
				user
					.updateProfile({displayName: username.value})
				.then(() => {
					user.getIdToken(/* forceRefresh */ true)
						.then((idToken) => {
							// Send token to your backend via HTTPS
							const data = user;
							data.token = idToken;

							dispatch(SIGN, data);
						}).catch((error) => {
							console.warn(error);
							setState({
								...state,
								error: error.message
						});
				  });
				})
				.catch(error => {
					console.warn(error);
					setState({
						...state,
						error: error.message
					});
				});
				})
			.catch(error => {
				console.warn(error);
				setState({
					...state,
					error: error.message
				});
			});
		}
	};

	const switchAuthMode = () => {
		setState({
			...state,
			controls: {
				...state.controls,
				email: {
					...state.controls.email,
					value: ''
				},
				username: {
					...state.controls.username,
					value: ''
				},
				password: {
					...state.controls.password,
					value: ''
				}
			},
			isSignup: !state.isSignup
		});
	};

	const formElArray = [];

	for (let key in state.controls) {
		// Don't push username input for signup state
		if (key !== 'username' || state.isSignup) {
			formElArray.push({
				id: key,
				...state.controls[key]
			});
		}
	}

	return (
		<div className={styles.login}>
        	<Title>{state.isSignup ? 'SIGNUP' : 'SIGNIN'}</Title>
			<form className={styles.form} onSubmit={onSubmitHandler}>
				{formElArray.map(input => (
					<Input
						key={input.id}
						elType={input.elType}
						label={input.id}
						elConfig={input.elConfig}
						value={input.value}
						errorMsg={input.errorMsg}
						invalid={!input.valid}
						touched={input.touched}
						changed={(e) => inputChangedHandler(e, input.id)}
					/>
				))}
				<div className={styles.submit}>
					<Button clicked={switchAuthMode}>
						{state.isSignup ? 'Signup' : 'Signin'}
					</Button>
					<Button>
						Submit
					</Button>
				</div>
				{state.error && <p className={styles.error}>{state.error}</p>}
			</form>
		</div>
	);
};

export default Login;
