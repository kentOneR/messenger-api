import React, {useEffect, useRef} 	from 'react';
import PropTypes 					from 'prop-types';

import Title 						from '../components/ui/Title';
import Post  						from '../components/Post';
import PostsList 					from '../components/PostsList';

import styles						from '../scss/containers/messages.module.scss';

import smoothscroll 				from 'smoothscroll-polyfill';

const Messages= ({user, posts}) => {

	const postsRef = useRef([]);

	/*
	* Hook: ComponentDidMount & ComponentDidUpdate
	*/
	useEffect(() => {
		// Compare prev/next posts from store
		// Scroll to bottom only on new post
		if (posts.length > postsRef.current.length) {
			// kick off the polyfill!
			smoothscroll.polyfill();

			const height = document.body.scrollHeight;

			height && window.scrollTo({
				top: height,
				left: 0,
				behavior: 'smooth'
			});
		}
		postsRef.current = [...posts];

	}, [user, posts]);


	return (
		<div className={styles.messages}>
			<Title>My messages</Title>
			<PostsList posts={posts} user={user}/>
			<Post user={user} />
		</div>
	);
};

Messages.propTypes = {
	user: PropTypes.object.isRequired,
	posts: PropTypes.array.isRequired
};

export default Messages;
