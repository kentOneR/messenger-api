import React 	  from 'react';

import {useStore} from '../hooks/store';

/**
* HOC: should be use to connect Class base Component to Store
* @params {JSX} [Component]
* @params {boolean} [shouldListen]
* @returns {JSX} [Component]
*/
const withHookStore = (Component, shouldListen = false) => {
	return (props) => {
	  const hookStore = useStore(shouldListen);
	  return <Component {...props} hookStore={hookStore} />;
	}
}

export default withHookStore;
