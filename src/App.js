import React, { Suspense, lazy } from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';

import Layout from './containers/Layout';
import Spinner from './components/ui/Spinner';

import {useStore}					from './hooks/store';

import './scss/app.scss'

const Home 		= lazy(() => import('./containers/Home')),
	  Login		= lazy(() => import('./containers/Login')),
	  Messages 	= lazy(() => import('./containers/Messages'));

const App = () => {

	const globalState = useStore(true)[0],
		  {
			  user 	= {},
			  posts = []
		  }	  		= globalState;

	const routes = (
		<Suspense fallback={<Spinner/>}>
			<Switch>
				<Route path="/" exact component={Home} />
				<Route path="/login" component={Login} />
				{!!user.token && <Route path="/messages" component={() => <Messages posts={posts} user={user} />}/>}
				<Redirect to="/" />
			</Switch>
		</Suspense>
	);

  return (
    <div className="App">
		<Layout isAuth={!!user.token}>
          {routes}
        </Layout>
    </div>
  );
}

export default App;
