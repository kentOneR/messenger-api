import {useState, useEffect} from 'react';

let globalState = {},
	listeners   = [],
	actions 	= {};

export const useStore = (shouldListen = false) => {
	const globalSetState = useState(globalState)[1];

	useEffect(() => {
		if (shouldListen) listeners.push(globalSetState);

		return () => {
			if (shouldListen) listeners = listeners.filter(listener => listener !== globalSetState);
		}
	}, [globalSetState, shouldListen]);

	const dispatch = (actionId, payload) => {
		const newState = actions[actionId](globalState, payload);

		globalState = {...globalState, ...newState};

		for (const listener of listeners) {
			listener(globalState);
		}
	};

	return [globalState, dispatch];
};

export const initStore = (userActions, initialState) => {
	if (initialState) {
		globalState = {...globalState, ...initialState};
	}

	actions = {...actions, ...userActions};
};
