import {initStore} 	from './store';
import {
	SIGN,
	SIGNOUT
}					from './actionTypes';

const configureUserStore = () => {
	const actions = {
		[SIGN]: (state, payload) => {
			const {email, displayName, token} = payload;

			return {
				...state,
				user: {
					email,
					username: displayName,
					token
				}
			};
		},
		[SIGNOUT]: (state) => {
			return {
				...state,
				user: {},
				posts: []
			}
		}

	};

	initStore(actions, {user: {}});
}

export default configureUserStore;
