import {initStore} 	from './store';
import {
	READ_POSTS,
	DELETE_POST
}					from './actionTypes';

const configurePostsStore = () => {
	const actions = {
		[READ_POSTS]: (state, payload) => {
			const posts = [...state.posts];

			// Only push new post
			for (let key in payload) {
				if (!posts.find(el => el.key === key)) {

					const {userName, userEmail, date, content, isPrivate} = payload[key];

					if (!isPrivate || (userEmail && userEmail.localeCompare(state.user.email) === 0)) {
						posts.push({
							key,
							userName,
							userEmail,
							date,
							content,
							isPrivate
						});
					}
				}
			}

			return {
				...state,
				posts: [...posts]
			};
		},
		[DELETE_POST]:(state, key) => {
			const posts = [...state.posts].filter(post => post.key !== key);

			return {
				...state,
				posts: [...posts]
			};
		},

	};

	initStore(actions, {posts: []});
}

export default configurePostsStore;
