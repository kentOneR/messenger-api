
/**
 * Check input validity from React State
 *
 * @param {string} [value] - input value
 * @param {object} [rules] - rules of input
 * @returns {boolean} [isValid]
 */
export const checkValidity = (value, rules) => {
	let isValid = true;

	if (!rules) {
	  return true;
	}

	if (rules.required && isValid) {
	  isValid = value.trim() !== '';
	}

	if (rules.minLength && isValid) {
	  isValid = value.length >= rules.minLength;
	}

	if (rules.isEmail && isValid) {
	  const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  isValid = pattern.test(value);
	}

	return isValid;
};


/**
 * Get current date from Date.now();
 * https://stackoverflow.com/questions/1531093/how-do-i-get-the-current-date-in-javascript
 *
 * @param {object} [date] - new Date();
 * @returns {string}
 */
export const getCurrentDate = (date) => {

	// return console.log(date);
	if (typeof date === 'undefined') {
		return '';
	}

	const today = date,
		  hour 	= String(today.getHours()).padStart(2, '0'),
		  min	= String(today.getMinutes()).padStart(2, '0'),
		  dd 	= String(today.getDate()).padStart(2, '0'),
    	  mm 	= String(today.getMonth() + 1).padStart(2, '0'), //January is 0!
		  yyyy 	= today.getFullYear();

	return `${dd}/${mm}/${yyyy} - ${hour}:${min}`;
}
