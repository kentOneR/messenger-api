import React 		from 'react';
import PropTypes 	from 'prop-types';

import styles 		from '../../scss/components/ui/close.module.scss';

const Close = ({onClick}) => (
	<span className={styles.close} onClick={onClick}/>
);

Close.propTypes = {
	onClick: PropTypes.func
};

export default Close;
