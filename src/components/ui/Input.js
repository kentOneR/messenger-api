import React 		from 'react';
import PropTypes	from 'prop-types';
import classNames 	from 'classnames';
import styles 		from '../../scss/components/ui/input.module.scss';

const Input = (props) => {

	const {
		changed 	= () => {},
		detail,
		elConfig 	= {},
		elStyle 	= {},
		elType: ElType,
		errorMsg,
		label,
		invalid,
		touched,
		value		= ''
	} = props;

	let inputEl,
		validationError;

	if (invalid && touched) {
		validationError = <p className={styles.error}>{errorMsg || 'Please enter a valid value!'}</p>;
	}

	switch (ElType) {
		case 'input':
		case 'textarea':
		case 'select':
			inputEl = (
				<ElType className={classNames({[styles.invalid]: invalid && touched})} style={{...elStyle}} value={value} onChange={changed} {...elConfig}>
					{elConfig.options && elConfig.options.map(op => (
						<option key={op.value} value={op.value}>{op.displayValue}</option>
					))}
				</ElType>
			);
			break;
		default:
			inputEl = <input className={classNames({[styles.invalid]: invalid && touched})} style={{...elStyle}} {...elConfig} value={value} onChange={changed} />;
	}

	return (
		<div className={[styles.input, styles[elConfig.type]].join(' ')}>
			{inputEl}
			{detail && <label>{label}</label>}
			{detail && <span>{detail}</span>}
			{validationError}
		</div>
	);
}

Input.propTypes = {
	changed: PropTypes.func.isRequired,
	elConfig: PropTypes.object.isRequired,
	elStyle: PropTypes.object,
	elType: PropTypes.string,
	invalid: PropTypes.bool,
	errorMsg: PropTypes.string,
	label: PropTypes.string,
	touch: PropTypes.bool,
	value: PropTypes.string,
	detail: PropTypes.string,
};

export default Input;
