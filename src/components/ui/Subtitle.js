import React 		from 'react';
import PropTypes	from 'prop-types';
import styles 		from '../../scss/components/ui/subtitle.module.scss';

const Subtitle = ({children}) => (
	<h2 className={styles.subtitle}>{children}</h2>
);

Subtitle.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.element
	])
};

export default Subtitle
