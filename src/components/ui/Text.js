import React 		from 'react';
import PropTypes	from 'prop-types';
import styles		from '../../scss/components/ui/text.module.scss';

const Text = ({children}) => (
	<p className={styles.text}>{children}</p>
);

Text.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.element
	])
};

export default Text;
