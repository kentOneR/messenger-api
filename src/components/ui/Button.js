import React  		from 'react';
import PropTypes	from 'prop-types';
import styles 		from '../../scss/components/ui/button.module.scss'

const Button = ({children, clicked, disabled}) => (
  	<button
		className={styles.button}
		onClick={clicked}
		disabled={disabled}
	>
		{children}
	</button>
);

Button.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.element
	]),
	clicked: PropTypes.func,
	disabled: PropTypes.bool
};

export default Button;
