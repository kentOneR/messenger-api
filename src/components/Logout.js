import React 		from 'react';
import {auth} 		from '../firebase';

import {useStore}	from '../hooks/store';
import {SIGNOUT}	from '../hooks/actionTypes';

import styles 		from '../scss/components/logout.module.scss';

const Logout = () => {

	const dispatch = useStore(false)[1];

	const logOutUser = (e) => {
		e.preventDefault();

		auth.signOut()
			.then(() => {
				dispatch(SIGNOUT);
			})
			.catch((error) => {
				console.warn(error);
			});
	}

	return <a className={styles.logout} href="/" onClick={logOutUser}>Logout</a>;
};

export default Logout;
