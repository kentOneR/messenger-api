import React 	 	from 'react';
import classNames	from 'classnames';
import PropTypes 	from 'prop-types';
import {NavLink} 	from 'react-router-dom';

import Logout		from './Logout';

import styles 		from '../scss/components/header.module.scss';

const Header = ({isAuth, isMount}) => (
	<header className={classNames(styles.header, {[styles['is-mount']]: isMount})}>
		<div className={styles.wrapper}>
			<nav>
				<ul className={styles.list}>
					<li><NavLink className={styles.link} to="/" exact>Home</NavLink></li>
					{!isAuth && <li><NavLink className={styles.link} to="/login">Login</NavLink></li>}
					{isAuth && <li><NavLink className={styles.link} to="/messages">Message</NavLink></li>}
				</ul>
			</nav>
			{isAuth && <Logout isAuth={isAuth} />}
		</div>
	</header>
);

Header.propTypes = {
	isAuth: PropTypes.bool.isRequired
};

export default Header;
