import React, {useState} 	from 'react';
import classNames			from 'classnames';
import PropTypes 			from 'prop-types';
import {database}			from '../firebase';

import Button 				from './ui/Button';
import Input 				from './ui/Input';

import {
	checkValidity,
	getCurrentDate
} 							from '../utils';

import styles 				from '../scss/components/post.module.scss';

const Post = ({user}) => {

	const [state, setState] = useState({
		controls: {
			content: {
				elType: 'textarea',
				elConfig: {
				  type: 'text'
				},
				elStyle: {
					height: ''
				},
				value: '',
				validation: {
				  required: true
				},
				valid: false,
				touched: false
			  },
			  switch: {
				elType: 'input',
				elConfig: {
				  type: 'checkbox',
				  defaultChecked: false
				},
				detail: 'Private'
			  }
		  },
		  error: false
	});

	const formElArray = [];

	for (let key in state.controls) {
		formElArray.push({
			id: key,
			...state.controls[key]
		});
	}

	const inputChangedHandler = ({target}, id) => {
		const updateState = {
			...state,
			controls: {
				...state.controls,
				[id]: {
					...state.controls[id],
					elStyle: {
						...state.controls[id].elStyle,
						height: `${target.scrollHeight}px`
					},
					value: target.value,
					valid: checkValidity(target.value, state.controls[id].validation),
					touched: true
				}
			}
		};

		if (target.type === 'checkbox') {
			updateState.controls[id].elConfig.defaultChecked = target.checked;
		}

		setState({...updateState});
	};

	const onSubmitHandler = (e) => {
		e.preventDefault();

		const {controls} = state,
			  userName	= user.username,
			  userEmail = user.email,
			  content 	= controls.content.value,
			  isPrivate	= controls.switch.elConfig.defaultChecked;

		database.ref('posts').push({
			userName,
			userEmail,
			date: getCurrentDate(new Date()),
			content,
			isPrivate
		});

		const updateState = {
			...state,
			controls: {
				...state.controls,
				content: {
				  ...state.controls.content,
				  elStyle: {
					...state.controls.content.elStyle,
					height: ''
				},
				  value: ''
				},
				switch: {
					...state.controls.switch,
					elConfig: {
						...state.controls.switch.elConfig,
						defaultChecked: false
					}
				}
			}
		};

		setState({...updateState});
	};

	return (
		<div
			className={classNames(styles.post, {
				[styles['is-fill']]: state.controls.content.value
			})}
		>
			<form onSubmit={onSubmitHandler}>
				{formElArray.map(input => (
					<Input
						key={input.id}
						elType={input.elType}
						label={input.id}
						detail={input.detail}
						elConfig={input.elConfig}
						elStyle={input.elStyle}
						value={input.value}
						invalid={!input.valid}
						touched={input.touched}
						changed={(e) => inputChangedHandler(e, input.id)}
					/>
				))}
				<Button>Submit</Button>
			</form>
		</div>
	);

};

Post.propTypes = {
	user: PropTypes.object.isRequired
};

export default Post;
