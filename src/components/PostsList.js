import React, {Component}	from 'react';
import PropTypes 			from 'prop-types';
import classNames			from 'classnames';
import {database} 			from '../firebase';

import withHookStore		from '../hoc/withHookStore';

import Close				from './ui/Close';
import Text 				from './ui/Text';

import {
	DELETE_POST,
	READ_POSTS
}							from '../hooks/actionTypes';

import styles 				from '../scss/components/posts-list.module.scss';

class PostsList extends Component {

	constructor(props) {
		super(props);

		this.dispatch = props.hookStore[1];
	}

	componentDidMount() {
		this.updatePostsList();
	}

	shouldComponentUpdate(nexProps) {
		return this.props.posts !== nexProps.posts;
	}

	componentWillUnmount() {
		// Disconnect Firebase on()
		this.firebaseRef.off();
	}

	render() {

		const {user, posts} = this.props;

		return (
			<div className={styles['posts-list']}>
				<ul>
					{posts.map(post => (
						<li
							id={post.key}
							key={post.key}
							className={classNames(styles['posts-item'], {
								[styles['is-mine']]: user.email === post.userEmail,
								[styles['is-private']]: post.isPrivate
							})}
						>
							<Text>{post.content}</Text>
							<div className={styles['posts-details']}>
								<span>{post.userName}</span>
								<span>{post.date}</span>
							</div>
							{user.email === post.userEmail && <Close onClick={this.onDeleteHandler}/>}
						</li>
					))}
				</ul>
			</div>
		);
	}

	/*
	 * Update global store on new posts
	 * Only if new key & post is not 'provite' or it's log user's post
 	 * @returns {array} [posts]
	*/
	updatePostsList = () => {
		const {user, posts} = this.props;

		this.firebaseRef = database.ref('posts');

		this.firebaseRef.on('value', (snapshot) => {
			const data = snapshot.val();
			for (let key in data) {
				/*
				 * IF IE in scope, should add polyfill
				 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find
				*/
				if (!posts.find(post => post.key === key)) {
					if (!data[key].isPrivate || (data[key].userEmail && data[key].userEmail.localeCompare(user.email) === 0)) {
						this.dispatch(READ_POSTS, data);
						break;
					}
				}
			}
		  }, (errorObject) => {
			console.warn('The read failed: ' + errorObject.code);
		});
	};

	/*
	 * Delete your own post on click
	*/
	onDeleteHandler = ({target}) => {
		const {id} = target.parentElement;

		if (id) {
			database.ref(`posts/${id}`)
				.remove()
				.then(() => {
					this.dispatch(DELETE_POST, id);
					console.log(`Post id:${id} removed`);
				})
				.catch((error) => {
					console.warn(error);
				});
		}
	};
};

PostsList.propTypes = {
	user: PropTypes.object.isRequired,
	posts: PropTypes.array.isRequired
};

export default withHookStore(PostsList, false);
